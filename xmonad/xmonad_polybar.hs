
-- Importar..................................................................................
import XMonad
import XMonad.Util.Run(spawnPipe)
import System.IO
import System.Exit
import XMonad.Util.SpawnOnce
import Data.Tree
import qualified XMonad.StackSet as W
import qualified Data.Map as M

import XMonad.Actions.CycleRecentWS
import XMonad.Actions.WithAll(sinkAll,killAll)
import XMonad.Actions.CopyWindow(kill1)
import XMonad.Actions.CycleWS
import XMonad.Actions.CycleWindows
import qualified XMonad.Actions.Submap as SM
import qualified XMonad.Actions.Search as S
import qualified XMonad.Actions.TreeSelect as TS
import qualified XMonad.Actions.DynamicWorkspaceOrder as DO

import XMonad.Hooks.DynamicLog (dynamicLogWithPP, wrap, xmobarPP, xmobarColor, shorten, PP(..))
import XMonad.Hooks.EwmhDesktops  -- for some fullscreen events, also for xcomposite in obs.
import XMonad.Hooks.ManageDocks (avoidStruts, docksEventHook, manageDocks, ToggleStruts(..))
import XMonad.Hooks.ManageHelpers (isFullscreen, doFullFloat, doCenterFloat)
import XMonad.Hooks.ServerMode
import XMonad.Hooks.SetWMName

import XMonad.Layout.ResizableTile
import XMonad.Layout.LayoutModifier
import XMonad.Layout.NoBorders
import XMonad.Layout.Renamed (renamed, Rename(Replace))
import XMonad.Layout.Spacing
import XMonad.Layout.Accordion
import XMonad.Layout.PerWorkspace
import XMonad.Layout.Tabbed
import XMonad.Layout.Minimize
-- import XMonad.Layout.Spiral
-- import XMonad.Layout.AvoidFloats
-- import XMonad.Layout.BinarySpacePartition


import XMonad.Prompt.Shell (shellPrompt)
import XMonad.Prompt
-- .........................................................................


-- variables...............................................................
myFocusFollowsMouse = False

myClickJustFocuses = True

myBorderWidth = 2

myNormalColor = "#333333"

myFocusColor = "#fef6c9"

myTerminal  = "kitty"

myEditor = myTerminal ++ " -e vim "

myFont = "xft:Roboto:bold:size=10"

myWorkspaces = ["1","2","3","4","5"]

myModMask = mod4Mask
-- .................................................................................

-- Keys..............................................................................

myKeys conf@(XConfig {XMonad.modMask = modm}) = M.fromList $

    -- launch a terminal
    [ ((modm,              xK_Return ), spawn $ myTerminal)

    -- Kill focused window
    , ((modm,                   xK_q ), kill1)
    
    -- Reconfigure & restart xmonad
    , ((modm,              xK_Escape ), spawn "xmonad --recompile; xmonad --restart")

-- Apps.............................................................................

    -- launch shell promp
    , ((modm,                   xK_p ), shellPrompt myXPConfig)

    -- Tree
    , ((modm,               xK_w ), treeselectAction tsDefaultConfig)

    -- launch rofi
    , ((modm,                   xK_d  ), spawn $ "rofi -show drun")

    -- vifm
    , ((modm,                    xK_a ), spawn $ "alacritty -e ranger")

    -- Firefox
    , ((modm .|. mod1Mask,                   xK_f  ), spawn $ "firefox")

    -- LibreWolf
    , ((modm .|. mod1Mask,                   xK_w  ), spawn $ "librewolf")

    -- Chromium
    , ((modm .|. mod1Mask,                   xK_c  ), spawn $ "chromium")

    -- Thunderbird
    , ((modm .|. mod1Mask,                   xK_t  ), spawn $ "thunderbird")
    
    -- Galculator
    , ((modm .|. mod1Mask,                   xK_g  ), spawn $ "galculator")

    -- Thunar
    , ((modm .|. shiftMask,                   xK_a  ), spawn $ "thunar")

    -- ncmpcpp
   , ((modm .|. mod1Mask,                   xK_n  ), spawn $ "urxvt -e ncmpcpp")

    -- Vim
    ,((modm .|. mod1Mask,                   xK_v  ), spawn $ myEditor)

    -- Emacs
    ,((modm .|. mod1Mask,                   xK_e  ), spawn $ "alacritty -e emacs")

-- Volume and brigt..................................................................

    -- lower volume
    , ((0                     , 0x1008FF11), spawn "amixer -q sset Master 5%-")

    -- Raise volume
    , ((0                     , 0x1008FF13), spawn "amixer -q -D pulse sset Master 5%+")

    -- Mute volume
    , ((0                     , 0x1008FF12), spawn "amixer -q -D pulse sset Master,0 toggle")

    -- lower bright
    , ((0                     , 0x1008FF03), spawn "xbacklight -dec 2 -time 0")

    -- Raise bright
    , ((0                     , 0x1008FF02), spawn "xbacklight -inc 2 -time 0")

    , ((0                     ,   xK_Print), spawn "xfce4-screenshooter")
    -- ncmpcpp
    , ((0                     , 0x1008FF81), spawn "urxvt -e ncmpcpp")
    -- Thunderbird
    , ((0                     , 0x1008FF19), spawn "thunderbird")
    -- Calculadora
    , ((0                     , 0x1008FF1d), spawn "galculator")
-- ....................................................................................

-- Tab and Bar.........................................................................
    
    , ((mod1Mask,               xK_bar ), cycleRecentWS [xK_Alt_L] xK_Tab xK_bar)
    , ((mod1Mask,               xK_Tab ), cycleRecentWindows [xK_Alt_L] xK_Tab xK_bar)
    , ((modm .|. controlMask,   xK_Tab ), nextWS)
    , ((modm .|. controlMask,   xK_bar ), prevWS)
    , ((modm .|. shiftMask,     xK_bar ), shiftToPrev >> prevWS)
    , ((modm .|. shiftMask,     xK_Tab ), shiftToNext >> nextWS)
    , ((modm .|. controlMask .|. shiftMask, xK_Tab ), DO.shiftTo Next HiddenNonEmptyWS)
    , ((modm .|. controlMask .|. shiftMask, xK_bar ), DO.shiftTo Prev HiddenNonEmptyWS)
    , ((modm,                   xK_Tab ), DO.moveTo Next HiddenNonEmptyWS)
    , ((modm,                   xK_bar ), DO.moveTo Prev HiddenNonEmptyWS)
-- .....................................................................................

-- Windows & layouts....................................................................
     -- Rotate through the available layout algorithms
    , ((modm,               xK_space ), sendMessage NextLayout)

    --  Reset the layouts on the current workspace to default
    , ((modm .|. shiftMask, xK_space ), setLayout $ XMonad.layoutHook conf)

    -- Resize viewed windows to the correct size
--    , ((modm,               xK_n     ), refresh)

    -- Move focus to the next window
    , ((modm,               xK_j     ), windows W.focusDown)

    -- Move focus to the previous window
    , ((modm,               xK_k     ), windows W.focusUp  )

    -- Move focus to the master window
    , ((modm,               xK_n     ), windows W.focusMaster  )

    -- Swap the focused window and the master window
    , ((modm .|. shiftMask,               xK_Return), windows W.swapMaster)

    -- Swap the focused window with the next window
    , ((modm .|. shiftMask, xK_j     ), windows W.swapDown  )

    -- Swap the focused window with the previous window
    , ((modm .|. shiftMask, xK_k     ), windows W.swapUp    )

    -- Shrink the master area
    , ((modm,               xK_h     ), sendMessage Shrink)

    -- Expand the master area
    , ((modm,               xK_l     ), sendMessage Expand)

    -- Shrink 
    , ((modm .|. shiftMask,    xK_h  ), sendMessage MirrorShrink)

    -- Expand 
    , ((modm .|. shiftMask,      xK_l ), sendMessage MirrorExpand)

    -- Push window back into tiling
    , ((modm,               xK_t     ), withFocused $ windows . W.sink)

    -- Increment the number of windows in the master area
    , ((modm              , xK_comma ), sendMessage (IncMasterN 1))

    -- Deincrement the number of windows in the master area
    , ((modm              , xK_period), sendMessage (IncMasterN (-1)))

    -- Toggle the status bar gap
    -- Use this binding with avoidStruts from Hooks.ManageDocks.
    -- See also the statusBar function from Hooks.DynamicLog.
    --
    , ((modm              , xK_f     ), sendMessage ToggleStruts)

    -- Search commands
    , ((modm, xK_s), SM.submap $ searchEngineMap $ S.promptSearch srchXPConfig)
    , ((modm .|. shiftMask, xK_s), SM.submap $ searchEngineMap $ S.selectSearch)
    ]
    ++

    --
    -- mod-[1..9], Switch to workspace N
    -- mod-shift-[1..9], Move client to workspace N
    --
    [((m .|. modm, k), windows $ f i)
        | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_5]
        , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]
    ++

    --
    -- mod-{w,e,r}, Switch to physical/Xinerama screens 1, 2, or 3
    -- mod-shift-{w,e,r}, Move client to screen 1, 2, or 3
    --
    [((m .|. modm, key), screenWorkspace sc >>= flip whenJust (windows . f))
        | (key, sc) <- zip [xK_m, xK_e, xK_r] [0..]
        , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]
-- .............................................................................


-- busqueda y promp................................................................

archwiki = S.searchEngine "archwiki" "https://wiki.archlinux.org/index.php?search="

searchEngineMap method = M.fromList $
      [ ((0, xK_g), method S.google)
      , ((0, xK_d), method S.duckduckgo)
      , ((0, xK_w), method S.wikipedia)
      , ((0, xK_i), method S.imdb)
      , ((0, xK_a), method archwiki)
      ]


myXPConfig :: XPConfig
myXPConfig = def
      { font                = myFont
      , bgColor             = "#241c1c"
      , fgColor             = "#d4d4d4"
      , bgHLight            = "#241c1c"
      , fgHLight            = "#96c0b7"
      , borderColor         = "#535974"
      , promptBorderWidth   = 0
      , position            = Top
--    , position            = CenteredAt { xpCenterY = 0.3, xpWidth = 0.3 }
      , height              = 20
      , historySize         = 256
      , historyFilter       = id
      , defaultText         = []
      , autoComplete        = Just 100000
      , showCompletionOnTab = False
      -- , searchPredicate     = isPrefixOf
      , alwaysHighlight     = True
      , maxComplRows        = Nothing      -- set to Just 5 for 5 rows
      }


srchXPConfig :: XPConfig
srchXPConfig = def
      { font                = myFont
      , bgColor             = "#241c1c"
      , fgColor             = "#d4d4d4"
      , bgHLight            = "#241c1c"
      , fgHLight            = "#00a6fb"
      , borderColor         = "#535974"
      , promptBorderWidth   = 0
      , position            = Top
--    , position            = CenteredAt { xpCenterY = 0.3, xpWidth = 0.3 }
      , height              = 20
      , historySize         = 0
      , historyFilter       = id
      , defaultText         = []
      , autoComplete        = Nothing
      , showCompletionOnTab = False
      -- , searchPredicate     = isPrefixOf
      , alwaysHighlight     = True
      , maxComplRows        = Nothing      -- set to Just 5 for 5 rows
      }

-- ........................................................


-- Action Tree..............................................................
tsDefaultConfig = TS.TSConfig { TS.ts_hidechildren = True
                              , TS.ts_background   = 0xdd374548
                              , TS.ts_font         = myFont
                              , TS.ts_node         = (0xffd0d0d0, 0xff241c1c )
                              , TS.ts_nodealt      = (0xffd0d0d0, 0xff241c1c )
                              , TS.ts_highlight    = (0xff00222b, 0xfffef6c9)
                              , TS.ts_extra        = 0xffd0d0d0
                              , TS.ts_node_width   = 200
                              , TS.ts_node_height  = 20
                              , TS.ts_originX      = 0
                              , TS.ts_originY      = 0
                              , TS.ts_indent       = 80
                              , TS.ts_navigate     = myTreeNavigation
                              }

myTreeNavigation = M.fromList
    [ ((0, xK_Escape),   TS.cancel)
    , ((0, xK_Return),   TS.select)
    , ((0, xK_space),    TS.select)
    , ((0, xK_Up),       TS.movePrev)
    , ((0, xK_Down),     TS.moveNext)
    , ((0, xK_Left),     TS.moveParent)
    , ((0, xK_Right),    TS.moveChild)
    , ((0, xK_k),        TS.movePrev)
    , ((0, xK_j),        TS.moveNext)
    , ((0, xK_h),        TS.moveParent)
    , ((0, xK_l),        TS.moveChild)
    ]


treeselectAction :: TS.TSConfig (X ()) -> X ()
treeselectAction a = TS.treeselectAction a
   [ Node (TS.TSNode "Sesión"    "Control Sesión"      (return())) 
        [ Node (TS.TSNode "Apagar" "Apagar el sistema" (spawn "systemctl -i poweroff")) []
        , Node (TS.TSNode "Reiniciar" "Reiniciar el sistema" (spawn "systemctl reboot"))[]
        , Node (TS.TSNode "Salir" "Salir de XMonad" (io (exitWith ExitSuccess)))[]
        , Node (TS.TSNode "Bloquear" "Bloquear pantalla" (spawn "xscreensaver-command -lock"))[]
        ]
    , Node (TS.TSNode "Configuración"   "Configuración XMonad" (return()))
        [ Node (TS.TSNode "XMonad"  "Editar XMonad" (spawn (myEditor ++ " ~/.xmonad/xmonad.hs")))[]
        , Node (TS.TSNode "XMobar"  "Editar Xmobar" (spawn (myEditor ++ " ~/.xmobarrc")))[]
        , Node (TS.TSNode "Dunst"  "Editar notificaciones" (spawn (myEditor ++ " ~/.config/dunst/dunstrc")))[]
        , Node (TS.TSNode "Picom"  "Editar compositor" (spawn (myEditor ++ " ~/.config/picom/picom.conf")))[]
        , Node (TS.TSNode "Xscreensaver"  "Protector de pantalla" (spawn "xscreensaver-demo"))[]
        ]
    
   , Node (TS.TSNode "Conección" "Configurar conecciones" (return ()))
       [ Node (TS.TSNode "Eth/Wifi" "Conecciones a internet"            (spawn "networkmanager_dmenu")) []
       , Node (TS.TSNode "Bluetooth" "Dispositivos Bluetooth" (spawn ("xterm -e bluetoothctl")))  []
       , Node (TS.TSNode "Áudio" "Dispositivos de entrada y salida" (spawn "pavucontrol"))  []
       ]
   ]
-- ................................................................................................

-- rules..................................................................
myManageHook =composeAll
    [ className =? "firefox"   --> doShift ("4")
    , className =? "Chromium"  --> doShift ("4")
    , className =? "LibreWolf"  --> doShift ("4")
    , className =? "Thunderbird"  --> doShift ("4") 
    , className =? "URxvt"     --> doCenterFloat 
    , className =? "URxvt"     --> doShift ("5") 
    , className =? "Galculator"     --> doCenterFloat 
    , className =? "R_x11"          --> doCenterFloat 
    , className =? "Gnuplot"          --> doCenterFloat 
    , className =? "Matplotlib"     --> doCenterFloat 
    , className =? "MEGAsync"     --> doCenterFloat 
    , className =? "Pavucontrol"     --> doCenterFloat 
    , className =? "XTerm"     --> doCenterFloat 
    , className =? "Xarchiver"     --> doFloat 
    , title  =? "Picture-in-Picture"     --> doCenterFloat 
    ]
-- --------------------------------------------------------------------

-- Layouts----------------------------------------------------------
mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw False (Border i i i i) True (Border i i i i) True


tall     = renamed [Replace "tall"]
           $ mySpacing 3
           $ ResizableTall 1 (3/100) (1/2) []

--binary  = renamed [Replace "binary"]
--           $ mySpacing 3
--           $ emptyBSP

myTabConfig = def { fontName            = "xft:roboto:regular:pixelsize=11"
                  , activeColor         = "#fef6c9"
                  , inactiveColor       = "#96c0b7"
                  , activeBorderColor   = "#292d3e"
                  , inactiveBorderColor = "#292d3e"
                  , activeTextColor     = "#241c1c"
                  , inactiveTextColor   = "#241c1c"
                  }

tabs     = renamed [Replace "tabs"]
           $ tabbed shrinkText myTabConfig

myLayoutHook = avoidStruts (tall ||| noBorders Accordion ||| Mirror tall)
myLayoutWeb = avoidStruts (tall ||| noBorders tabs ||| noBorders Full)
myLayoutRead = avoidStruts (noBorders tabs ||| tall ||| Mirror tall)
-- .....................................................................................
-- Autoarranque-----------------------------------------------------------------------
myStartupHook :: X () 
myStartupHook = do
        spawnOnce "systemctl --user start dunst.service &"
        spawnOnce "~/.xmonad/autostart.sh"
        spawnOnce "~/.config/polybar/launch_ewmh.sh"
-- .....................................................................................


-- Main.................................................................................
main = do   
    xmproc <- spawnPipe "xmobar"
    xmonad $ ewmh def 
        { layoutHook = onWorkspace "4" myLayoutWeb $
                       onWorkspace "2" myLayoutRead $
                       myLayoutHook
        , modMask = myModMask     -- Rebind Mod to the Windows key
        , startupHook        = myStartupHook
        , handleEventHook    = serverModeEventHookCmd
                                <+> serverModeEventHook
                                <+> docksEventHook
        , manageHook = ( isFullscreen --> doFullFloat ) <+> manageDocks <+> myManageHook
        , focusedBorderColor = myFocusColor
        , normalBorderColor = myNormalColor
        , borderWidth = myBorderWidth
        , focusFollowsMouse = myFocusFollowsMouse
        , clickJustFocuses = myClickJustFocuses
        , workspaces = myWorkspaces
        , keys = myKeys
        } 
