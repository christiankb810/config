#!/bin/bash

# Custom Rofi Script

# Launch Rofi
MENU="$(rofi -no-lazy-grab -sep "|" -dmenu -i -p 'System :' \
-theme ~/.config/rofi/slate-menu.rasi \
-lines 4 \
<<< "   Apagar|  Reiniciar|   Salir|  Bloquear")"
case "$MENU" in
  *Apagar) systemctl -i poweroff;;
  *Reiniciar) systemctl reboot;;
  *Salir) bspc quit;;
  *Bloquear) xscreensaver-command -lock
esac

