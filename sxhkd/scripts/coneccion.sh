#!/bin/bash

# Custom Rofi Script

# Launch Rofi
MENU="$(rofi -no-lazy-grab -sep "|" -dmenu -i -p 'Coneccion :' \
-theme ~/.config/rofi/slate-menu.rasi \
-lines 3 \
<<< "   Wifi|  Bluetooth|   Sonido")"
case "$MENU" in
  *Wifi) networkmanager_dmenu;;
  *Bluetooth) xterm -e bluetoothctl;;
  *Sonido) pavucontrol;;
esac

