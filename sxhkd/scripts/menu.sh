#!/bin/bash

# Custom Rofi Script

# Launch Rofi
MENU="$(rofi -no-lazy-grab -sep "|" -dmenu -i -p 'Menu:' \
-lines 3 \
-theme ~/.config/rofi/slate-menu.rasi \
<<< "   Sesión|  Conección|  Configuración")"
case "$MENU" in
  *Sesión) ~/.config/sxhkd/scripts/system.sh;;
  *Conección) ~/.config/sxhkd/scripts/coneccion.sh;;
  *Configuración) ~/.config/sxhkd/scripts/config.sh;;
esac

