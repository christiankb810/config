#!/bin/bash

# Custom Rofi Script

# Launch Rofi
MENU="$(rofi -no-lazy-grab -sep "|" -dmenu -i -p 'Configuracion :' \
-theme ~/.config/rofi/slate-menu.rasi \
-lines 4 \
<<< "   Bspwm|  Sxhkd|  Polybar|  Xscreensaver")"
case "$MENU" in
  *Bspwm) kitty -e vim  ~/.config/bspwm/bspwmrc;;
  *Sxhkd) kitty -e vim  ~/.config/sxhkd/sxhkdrc;;
  *Polybar) kitty -e vim  ~/.config/polybar/config.ini;;
  *Xscreensaver) xscreensaver-demo;;
esac

