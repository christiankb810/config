#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

PS1='\[\e[32m\]\u\[\e[34m\]@\H:\[\e[36m\]\W\[\e[00m\]$ '

export HISTCONTROL=ignoredups

# Alias
alias ls='exa -l'
alias la='exa -la'
alias clear='[ $[$RANDOM % 10] = 0  ] && timeout 3 sl || clear'
alias ..='cd ..'
alias rm='rm -i'
alias mv='mv -i'
alias autoremove='sudo pacman -R $(pacman -Qdtq)'
alias autoclean='sudo pacman -Scc'
alias search='pacman -Ss'
alias upgrade='sudo pacman -Syu'
alias grep='grep --color=auto'
alias df='df -h'

alias clone='git clone'
alias push='git push'
alias status='git status'
alias commit='git commit'
alias add='git add'
alias pull='git pull'
