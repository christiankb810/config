#!/bin/bash

waylogout \
	--hide-cancel \
	--screenshots \
	--font="Baloo 2" \
	--effect-blur=7x5 \
	--indicator-thickness=20 \
	--ring-color=888888aa \
	--inside-color=88888866 \
	--text-color=eaeaeaaa \
	--line-color=00000000 \
	--ring-selection-color=80FFE6aa \
	--inside-selection-color=80FFE666 \
	--text-selection-color=eaeaeaaa \
	--line-selection-color=00000000 \
	--lock-command="swaylock -f -c 000000" \
	--logout-command="swaymsg exit" \
	--suspend-command="systemctl suspend" \
	--hibernate-command="echo hibernate" \
	--poweroff-command="systemctl -i poweroff" \
	--reboot-command="systemctl reboot" \
	--switch-user-command="echo switch" \
	--selection-label
