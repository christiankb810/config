# The following lines were added by compinstall
zstyle :compinstall filename '/home/naitsirhc/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall
# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
bindkey -v
# End of lines configured by zsh-newuser-install

PROMPT='%F{cyan}%B%~%b%f %F{red}>%f '

setopt COMPLETE_ALIASES
zstyle ':completion:*' menu select
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=66'

# create a zkbd compatible hash;
# to add other keys to this hash, see: man 5 terminfo
typeset -g -A key

key[Home]="${terminfo[khome]}"
key[End]="${terminfo[kend]}"
key[Insert]="${terminfo[kich1]}"
key[Backspace]="${terminfo[kbs]}"
key[Delete]="${terminfo[kdch1]}"
key[Up]="${terminfo[kcuu1]}"
key[Down]="${terminfo[kcud1]}"
key[Left]="${terminfo[kcub1]}"
key[Right]="${terminfo[kcuf1]}"
key[PageUp]="${terminfo[kpp]}"
key[PageDown]="${terminfo[knp]}"
key[Shift-Tab]="${terminfo[kcbt]}"

# setup key accordingly
[[ -n "${key[Home]}"       ]] && bindkey -- "${key[Home]}"      beginning-of-line
[[ -n "${key[End]}"        ]] && bindkey -- "${key[End]}"       end-of-line
[[ -n "${key[Insert]}"     ]] && bindkey -- "${key[Insert]}"    overwrite-mode
[[ -n "${key[Backspace]}"  ]] && bindkey -- "${key[Backspace]}" backward-delete-char
[[ -n "${key[Delete]}"     ]] && bindkey -- "${key[Delete]}"    delete-char
[[ -n "${key[Up]}"         ]] && bindkey -- "${key[Up]}"        up-line-or-history
[[ -n "${key[Down]}"       ]] && bindkey -- "${key[Down]}"      down-line-or-history
[[ -n "${key[Left]}"       ]] && bindkey -- "${key[Left]}"      backward-char
[[ -n "${key[Right]}"      ]] && bindkey -- "${key[Right]}"     forward-char
[[ -n "${key[PageUp]}"     ]] && bindkey -- "${key[PageUp]}"    beginning-of-buffer-or-history
[[ -n "${key[PageDown]}"   ]] && bindkey -- "${key[PageDown]}"  end-of-buffer-or-history
[[ -n "${key[Shift-Tab]}"  ]] && bindkey -- "${key[Shift-Tab]}" reverse-menu-complete

# Finally, make sure the terminal is in application mode, when zle is
# # active. Only then are the values from $terminfo valid.
if (( ${+terminfo[smkx]} && ${+terminfo[rmkx]}  )); then
        autoload -Uz add-zle-hook-widget
        function zle_application_mode_start { echoti smkx  }
        function zle_application_mode_stop { echoti rmkx  }
        add-zle-hook-widget -Uz zle-line-init zle_application_mode_start
        add-zle-hook-widget -Uz zle-line-finish zle_application_mode_stop
fi

source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh

# Alias
alias ls='exa -l'
alias la='exa -la'
alias clear='[ $[$RANDOM % 12] = 0  ] && timeout 4 cmatrix || clear'
alias ..='cd ..'
alias rm='rm -i'
alias mv='mv -i'
alias order66='sudo pacman -R $(pacman -Qdtq)'
alias autoclean='sudo pacman -Scc'
alias search='pacman -Ss'
alias update='sudo pacman -Syu'
alias grep='grep --color=auto'
alias df='df -h'
alias cache='sudo sysctl vm.drop_caches=3'

alias clone='git clone'
alias push='git push'
alias status='git status'
alias commit='git commit'
alias add='git add'
alias pull='git pull'
