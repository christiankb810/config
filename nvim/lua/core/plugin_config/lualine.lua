require('lualine').setup {
    options = {
        icons_enable = true,
        theme = 'spaceduck',
    },
    sections = {
        lualine_a = {
            {
                'filename',
                path = 1,
            }
        }
    }
}
