require'nvim-treesitter.configs'.setup {
    ensure_installed = {"c", "lua", "rust", "vim", "python", "r", "org"},
    sync_install = false,
    auto_install = true,
    ignore_install = { "latex" },
    highlight = {
        enable = true,
        additional_vim_regex_highlighting = {'org'},
    }
}
