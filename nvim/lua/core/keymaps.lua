vim.opt.laststatus = 2
vim.opt.cursorline = true
vim.opt.backspace = '2'
vim.opt.autoread = true
vim.opt.showcmd = true
vim.opt.backup = false
vim.opt.swapfile = false
vim.opt.termguicolors = true

vim.opt.path:remove "/usr/include"
vim.opt.path:append("/home/naitsirhc/Documentos/Clases/**")
vim.opt.path:append("/home/naitsirhc/Documentos/CursosUdG/**")
vim.opt.path:append("/home/naitsirhc/Progra/**")
vim.opt.path:append("/home/naitsirhc/Plantillas/**")
vim.opt.ignorecase = true
vim.opt.smartcase = true
vim.opt.incsearch = true
vim.opt.hlsearch = true

vim.opt.expandtab = true
vim.opt.shiftwidth = 4
vim.opt.tabstop = 4
vim.opt.softtabstop = 4
vim.opt.smarttab = true
vim.opt.nu = true
vim.opt.rnu = true

if vim.g.neovide then
  vim.keymap.set('n', '<C-S-s>', ':w<CR>') -- Save
  vim.keymap.set('v', '<C-S-c>', '"+y') -- Copy
  vim.keymap.set('n', '<C-S-v>', '"+P') -- Paste normal mode
  vim.keymap.set('v', '<C-S-v>', '"+P') -- Paste visual mode
  vim.keymap.set('c', '<C-S-v>', '<C-R>+') -- Paste command mode
  vim.keymap.set('i', '<C-S-v>', '<ESC>l"+Pli') -- Paste insert mode
end

-- Allow clipboard copy paste in neovim
vim.api.nvim_set_keymap('', '<C-S-v>', '+p<CR>', { noremap = true, silent = true})
vim.api.nvim_set_keymap('!', '<C-S-v>', '<C-R>+', { noremap = true, silent = true})
vim.api.nvim_set_keymap('t', '<C-S-v>', '<C-R>+', { noremap = true, silent = true})
vim.api.nvim_set_keymap('v', '<C-S-v>', '<C-R>+', { noremap = true, silent = true})


vim.keymap.set('n', '<leader>h', ':nohlsearch<CR>')
vim.keymap.set('n', '<leader>f', ':find ')

vim.keymap.set('n', '<leader>p', ':!python %<CR>')

vim.keymap.set('n', '<leader>g', ':!gnuplot %<CR>')

vim.keymap.set('n', '<F4>c', ':!gcc -o %< -lm -lgsl -lgslcblas %<CR>')
vim.keymap.set('n', '<F5>c', ':!gcc -o %< %<CR>')
-- vim.keymap.set('n', '<F5>c', ':!gcc -o %< -lm %<CR>')
vim.keymap.set('n', '<F6>c', ':!./%< %<CR>')
