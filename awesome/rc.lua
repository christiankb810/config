-- If LuaRocks is installed, make sure that packages installed through it are
-- found (e.g. lgi). If LuaRocks is not installed, do nothing.
pcall(require, "luarocks.loader")

-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")
-- Widget and layout library
local wibox = require("wibox")
local vicious = require("vicious")
vicious.contrib = require"vicious.contrib"
local volume_widget = require('awesome-wm-widgets.volume-widget.volume')
-- Theme handling library
local beautiful = require("beautiful")
-- Notification library
local naughty = require("naughty")
local menubar = require("menubar")
local hotkeys_popup = require("awful.hotkeys_popup")
local cairo = require("lgi").cairo
-- Create a surface
local img = cairo.ImageSurface.create(cairo.Format.ARGB32, 50, 50)
os.setlocale(os.getenv("LANG"))
--
-- -- Create a context
local cr  = cairo.Context(img)
-- Enable hotkeys help widget for VIM and other apps
-- when client with a matching name is opened:
require("awful.hotkeys_popup.keys")

-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                     title = "Valió gaver, there were errors during startup!",
                     text = awesome.startup_errors })
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({ preset = naughty.config.presets.critical,
                         title = "Oops, an error happened!",
                         text = tostring(err) })
        in_error = false
    end)
end
-- }}}

-- {{{ Variable definitions
-- Themes define colours, icons, font and wallpapers.
local theme_path = string.format("%s/.config/awesome/themes/%s/theme.lua", os.getenv("HOME"), "default")
beautiful.init(theme_path)

-- This is used later as the default terminal and editor to run.
terminal = "kitty"
editor = os.getenv("EDITOR") or "vim"
editor_cmd = terminal .. " -e " .. editor

modkey = "Mod4"

-- Table of layouts to cover with awful.layout.inc, order matters.
awful.layout.layouts = {
    awful.layout.suit.tile,
    awful.layout.suit.tile.bottom,
    awful.layout.suit.spiral,
    awful.layout.suit.floating,
    -- awful.layout.suit.tile.left,
    -- awful.layout.suit.max.fullscreen,
    -- awful.layout.suit.max,
    -- awful.layout.suit.tile.top,
    -- awful.layout.suit.fair.horizontal,
    -- awful.layout.suit.fair,
    -- awful.layout.suit.spiral.dwindle,
    --awful.layout.suit.corner.nw,
    -- awful.layout.suit.corner.ne,
    -- awful.layout.suit.corner.sw,
    -- awful.layout.suit.corner.se,
}
-- }}}

-- {{{ Menu
-- Create a launcher widget and a main menu
myawesomemenu = {
   { "Apagar", "systemctl -i poweroff" },
   { "Reiniciar", "systemctl reboot" },
   { "Quitar", function() awesome.quit() end },
   { "Restart Awesome", awesome.restart },
   { "Edit config", editor_cmd .. " " .. awesome.conffile },
}

myconectionmenu = {
    {"Wifi", "networkmanager_dmenu"},
    {"Bluetooth",function() awful.spawn("xterm -e bluetoothctl") end},
    {"Sonido", function() awful.spawn("pavucontrol") end},
    {"Screensaver", "xscreensaver-demo"},
}

mymainmenu = awful.menu({ items = { { "Awesome", myawesomemenu, beautiful.awesome_icon },
                                    { "Coneccion", myconectionmenu },
                                  }
                        })

mylauncher = awful.widget.launcher({ image = beautiful.awesome_icon,
                                     menu = mymainmenu })

-- Menubar configuration
menubar.utils.terminal = terminal -- Set the terminal for applications that require it
-- }}}

-- Keyboard map indicator and switcher
mykeyboardlayout = awful.widget.keyboardlayout()

-- {{{ Wibar
-- Create a textclock widget
netwidget = wibox.widget.textbox()
vicious.register(netwidget, vicious.widgets.net, "  ${wlp2s0 down_kb}    ${wlp2s0 up_kb}")
netwidget:buttons(awful.util.table.join(
    awful.button({}, 1, function () awful.util.spawn("networkmanager_dmenu") end)
))

mynet = wibox.widget{
    {
        netwidget,
        left = 2,
        right = 8,
        widget = wibox.container.margin
    },
    -- fg = "#5c6b73",
    widget = wibox.container.background
}

mytextclock = wibox.widget {
    {
        wibox.widget.textclock ("  %a %d %b,  %l:%M "),
        left = 6,
        right = 6,
        widget = wibox.container.margin
    },
    bg = "#DE5283",
    fg = "#ffffff",
    shape = gears.shape.rounded_rect,
    widget = wibox.container.background
}

month_calendar = awful.widget.calendar_popup.month({position="tr", opacity=0.95, bg="#0f1108"})

mytextclock:connect_signal("button::press", 
    function(_, _, _, button)
        if button == 1 then month_calendar:toggle() end
    end)

-- Create a wibox for each screen and add it
local taglist_buttons = gears.table.join(
                    awful.button({ }, 1, function(t) t:view_only() end),
                    awful.button({ modkey }, 1, function(t)
                                              if client.focus then
                                                  client.focus:move_to_tag(t)
                                              end
                                          end),
                    awful.button({ }, 3, awful.tag.viewtoggle),
                    awful.button({ modkey }, 3, function(t)
                                              if client.focus then
                                                  client.focus:toggle_tag(t)
                                              end
                                          end),
                    awful.button({ }, 4, function(t) awful.tag.viewnext(t.screen) end),
                    awful.button({ }, 5, function(t) awful.tag.viewprev(t.screen) end)
                )

local tasklist_buttons = gears.table.join(
                     awful.button({ }, 1, function (c)
                                              if c == client.focus then
                                                  c.minimized = true
                                              else
                                                  c:emit_signal(
                                                      "request::activate",
                                                      "tasklist",
                                                      {raise = true}
                                                  )
                                              end
                                          end),
                     awful.button({ }, 3, function()
                                              awful.menu.client_list({ theme = { width = 250 } })
                                          end),
                     awful.button({ }, 4, function ()
                                              awful.client.focus.byidx(1)
                                          end),
                     awful.button({ }, 5, function ()
                                              awful.client.focus.byidx(-1)
                                          end))


awful.screen.connect_for_each_screen(function(s)
    -- Each screen has its own tag table.
    awful.tag({"", "", "", "", "", ""}, s, awful.layout.layouts[1])
    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()
    -- Create an imagebox widget which will contain an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = wibox.widget {
        {
            awful.widget.layoutbox(s),
            left=6,
            right=6,
            top=4,
            bottom=4,
            widget=wibox.container.margin
        },
        bg='#374548',
        shape=gears.shape.circle,
        widget=wibox.container.background
    }
    s.mylayoutbox:buttons(gears.table.join(
                           awful.button({ }, 1, function () awful.layout.inc( 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(-1) end),
                           awful.button({ }, 4, function () awful.layout.inc( 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(-1) end)))
    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist{
    screen = s,
    filter = awful.widget.taglist.filter.all,
    widget_template = {
        {
            {
                {
                    {
                        id = 'icon_role',
                        widget = wibox.widget.imagebox,
                    },
                    margins = 1,
                    widget = wibox.container.margin,
                },
                {
                    id = 'text_role',
                    widget = wibox.widget.textbox,
                },
                layout = wibox.layout.fixed.horizontal,
            },
            left = 16,
            right = 16,
            widget = wibox.container.margin,
        },
        id = 'background_role',
        widget = wibox.container.background,
        update_callback = function(self, c3, index, objects)
            self:get_children_by_id('icon_role')[1].markup = '<b> '..index..' </b>'
        end,
    },
    buttons= taglist_buttons
    }
    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist {
        screen  = s,
        filter  = awful.widget.tasklist.filter.currenttags,
        buttons = tasklist_buttons,
        layout = {
            spacing_widget = {
                {
                    forced_width = 5,
                    forced_height = 22,
                    thickness     = 1,
                    widget = wibox.widget.separator
                },
                valign = 'center',
                halign = 'center',
                widget = wibox.container.place,
            },
            spacing = 1,
            layout = wibox.layout.flex.horizontal,
        },
        widget_template = {
            {
                wibox.widget.base.make_widget(),
                forced_height = 1,
                id            = 'background_role',
                widget        = wibox.container.background,
            },
            {
                {
                    id     = 'clienticon',
                    widget = awful.widget.clienticon,
                },
                margins = 5,
                widget  = wibox.container.margin
            },
            nil,
            create_callback = function(self, c, index, objects)
                self:get_children_by_id('clienticon')[1].client = c
            end,
            layout = wibox.layout.align.vertical,
        },
    }

    -- Create the wibox
    s.mywibox = awful.wibar({ position = "top", screen = s, height=20, color = '#0f1108', opacity=0.96})

    -- Add widgets to the wibox
    s.mywibox:setup {
        layout = wibox.layout.align.horizontal,
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
            -- mylauncher,
            s.mytaglist,
            s.mypromptbox,
        },
        s.mytasklist, -- Middle widget
        { -- Right widgets
            spacing = 2,
            layout = wibox.layout.fixed.horizontal,
            s.mylayoutbox,
            mynet,
            volume_widget{
                type='bar'
            },
            mytextclock,
        },
    }
end)
-- }}}

-- {{{ Mouse bindings
root.buttons(gears.table.join(
    awful.button({ }, 3, function () mymainmenu:toggle() end),
    awful.button({ }, 4, awful.tag.viewnext),
    awful.button({ }, 5, awful.tag.viewprev),
    awful.button({modkey}, 1, awful.client.move)
))
-- }}}

-- {{{ Key bindings
globalkeys = gears.table.join(
    -- Volume Keys
    awful.key({}, "XF86AudioLowerVolume", function ()
        awful.util.spawn("amixer -q -D pulse sset Master 5%-", false)
    end),
    awful.key({}, "XF86AudioRaiseVolume", function ()
        awful.util.spawn("amixer -q -D pulse sset Master 5%+", false)
    end),
    awful.key({}, "XF86AudioMute", function ()
        awful.util.spawn("amixer -D pulse sset Master 1+ toggle", false)
    end),
    -- Media 
    awful.key({}, "XF86AudioPlay", function()
        awful.util.spawn("playerctl play-pause", false)
    end),
    awful.key({}, "XF86AudioNext", function()
        awful.util.spawn("playerctl next", false)
    end),
    awful.key({}, "XF86AudioPrev", function()
        awful.util.spawn("playerctl previous", false)
    end),
    awful.key({}, "Print", function ()
        awful.util.spawn("xfce4-screenshooter", false)
    end),
    awful.key({modkey,      "Mod1"}, "f", function() awful.spawn("firefox") end,
              {description = "Firefox", group = "launcher"}),
    awful.key({modkey,      "Mod1"}, "w", function() awful.spawn("librewolf") end,
              {description = "Librewolf", group = "launcher"}),
    awful.key({modkey,      "Mod1"}, "c", function() awful.spawn("chromium") end,
              {description = "Chromium", group = "launcher"}),
    awful.key({}, "XF86Mail", function() awful.spawn("thunderbird") end,
              {description = "Thunderbird", group = "launcher"}),
    awful.key({}, "XF86Calculator", function() awful.spawn("galculator") end,
              {description = "Galculator", group = "launcher"}),
    awful.key({modkey,      "Mod1"}, "v", function() awful.spawn( terminal.." -e vim") end,
              {description = "Vim", group = "launcher"}),
    awful.key({modkey,            }, "a", function() awful.spawn("alacritty -e ranger" ) end,
              {description = "Ranger", group = "launcher"}),
    awful.key({modkey,            }, "d", function() awful.spawn("rofi -show drun" ) end,
              {description = "Rofi", group = "launcher"}),
    awful.key({modkey,        "Shift"}, "a", function() awful.spawn("pcmanfm") end,
              {description = "PCManFM", group = "launcher"}),
    awful.key({}, "XF86Tools", function() awful.spawn("urxvt -e ncmpcpp") end,
              {description = "ncmpcpp", group = "launcher"}),
    awful.key({ modkey,           }, "s",      hotkeys_popup.show_help,
              {description="show help", group="awesome"}),

    awful.key({ modkey,           }, "h", function () awful.tag.incmwfact(-0.05) end,
              {description = "decrease master width factor", group = "layout"}),
    awful.key({ modkey,           }, "l", function () awful.tag.incmwfact( 0.05) end,
              {description = "increase master width factor", group = "layout"}),
    awful.key({ modkey,           }, "k", function () awful.client.focus.byidx(-1) end,
              {description = "focus the next client", group = "client"}),
    awful.key({ modkey,           }, "j", function () awful.client.focus.byidx(1) end,
              {description = "focus the previous client", group = "client"}),

    awful.key({ modkey,           }, "|",   awful.tag.viewprev,
              {description = "view previous", group = "tag"}),
    awful.key({ modkey,           }, "Tab",  awful.tag.viewnext,
              {description = "view next", group = "tag"}),
    awful.key({ "Mod1",           }, "|", awful.tag.history.restore,
              {description = "go back", group = "tag"}),
    -- Win+Alt+Left/Right: move client to prev/next tag and switch to it
	awful.key({ modkey, "Shift" }, "|",
		function ()
			-- get current tag
			local t = client.focus and client.focus.first_tag or nil
			if t == nil then
				return
			end
			-- get previous tag (modulo 9 excluding 0 to wrap from 1 to 9)
			local tag = client.focus.screen.tags[(t.index - 2) % 5 + 1]
			awful.client.movetotag(tag)
			awful.tag.viewprev()
		end,
			{description = "move client to previous tag and switch to it", group = "layout"}),
	awful.key({ modkey, "Shift" }, "Tab",
		function ()
			-- get current tag
			local t = client.focus and client.focus.first_tag or nil
			if t == nil then
				return
			end
			-- get next tag (modulo 9 excluding 0 to wrap from 9 to 1)
			local tag = client.focus.screen.tags[(t.index % 5) + 1]
			awful.client.movetotag(tag)
			awful.tag.viewnext()
		end,
			{description = "move client to next tag and switch to it", group = "layout"}),

    awful.key({ modkey,           }, "w", function () mymainmenu:show() end,
              {description = "show main menu", group = "awesome"}),

    -- Layout manipulation
    awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx(1)    end,
              {description = "swap with down client by index", group = "client"}),
    awful.key({ modkey, "Shift"   }, "k",     function () awful.client.swap.byidx(-1) end,
              {description = "swap with up client by index", group = "client"}),

    awful.key({ modkey,           }, "u", awful.client.urgent.jumpto,
              {description = "jump to urgent client", group = "client"}),
    awful.key({ "Mod1", "Shift"   }, "|",
        function ()
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end,
        {description = "go back", group = "client"}),
    -- awful.key({"Mod1",          }, "|",
    --     function()
    --         local screen = awful.screen.focused()
    --         local tag = awful.tag.history.restore (screen, previous)
    --         if tag then
    --             tag:view_only()
    --         end
    --     end,
    --     {description = "focus the previous tag", group= "tag"}),

    -- Standard program
    awful.key({ modkey,           }, "Return", function () awful.spawn(terminal) end,
              {description = "open a terminal", group = "launcher"}),
    awful.key({ modkey, "Control" }, "r", awesome.restart,
              {description = "reload awesome", group = "awesome"}),
    awful.key({ modkey,           }, ".",     function () awful.tag.incnmaster( 1, nil, true) end,
              {description = "increase the number of master clients", group = "layout"}),
    awful.key({ modkey,           }, ",",     function () awful.tag.incnmaster(-1, nil, true) end,
              {description = "decrease the number of master clients", group = "layout"}),
    awful.key({ modkey, "Control" }, "l",     function () awful.tag.incncol( 1, nil, true)    end,
              {description = "increase the number of columns", group = "layout"}),
    awful.key({ modkey, "Control" }, "h",     function () awful.tag.incncol(-1, nil, true)    end,
              {description = "decrease the number of columns", group = "layout"}),
    awful.key({ modkey,           }, "space", function () awful.layout.inc( 1)                end,
              {description = "select next", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "space", function () awful.layout.inc(-1)                end,
              {description = "select previous", group = "layout"}),

    awful.key({ modkey, "Shift" }, "n",
              function ()
                  local c = awful.client.restore()
                  -- Focus restored client
                  if c then
                    c:emit_signal(
                        "request::activate", "key.unminimize", {raise = true}
                    )
                  end
              end,
              {description = "restore minimized", group = "client"}),

    -- Prompt
    awful.key({ modkey },            "r",     function () awful.screen.focused().mypromptbox:run() end,
              {description = "run prompt", group = "launcher"}),

    awful.key({ modkey }, "x",
              function ()
                  awful.prompt.run {
                    prompt       = "Run Lua code: ",
                    textbox      = awful.screen.focused().mypromptbox.widget,
                    exe_callback = awful.util.eval,
                    history_path = awful.util.get_cache_dir() .. "/history_eval"
                  }
              end,
              {description = "lua execute prompt", group = "awesome"}),
    -- Menubar
    awful.key({ modkey }, "p", function() menubar.show() end,
              {description = "show the menubar", group = "launcher"})
)

clientkeys = gears.table.join(
    awful.key({ modkey,           }, "f",
        function (c)
            c.fullscreen = not c.fullscreen
            c:raise()
        end,
        {description = "toggle fullscreen", group = "client"}),
    awful.key({ modkey,    }, "q",      function (c) c:kill()                         end,
              {description = "close", group = "client"}),
    awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle                     ,
              {description = "toggle floating", group = "client"}),
    awful.key({ modkey, "Shift" }, "Return", function (c) c:swap(awful.client.getmaster()) end,
              {description = "move to master", group = "client"}),
    awful.key({ modkey,           }, "o",      function (c) c:move_to_screen()               end,
              {description = "move to screen", group = "client"}),
    awful.key({ modkey,           }, "t",      function (c) c.ontop = not c.ontop            end,
              {description = "toggle keep on top", group = "client"}),
    awful.key({ modkey,           }, "n",
        function (c)
            -- The client currently has the input focus, so it cannot be
            -- minimized, since minimized clients can't have the focus.
            c.minimized = true
        end ,
        {description = "minimize", group = "client"}),
    awful.key({ modkey,           }, "m",
        function (c)
            c.maximized = not c.maximized
            c:raise()
        end ,
        {description = "(un)maximize", group = "client"}),
    awful.key({ modkey, "Control" }, "m",
        function (c)
            c.maximized_vertical = not c.maximized_vertical
            c:raise()
        end ,
        {description = "(un)maximize vertically", group = "client"}),
    awful.key({ modkey, "Shift"   }, "m",
        function (c)
            c.maximized_horizontal = not c.maximized_horizontal
            c:raise()
        end ,
        {description = "(un)maximize horizontally", group = "client"})
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it work on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 5 do
    globalkeys = gears.table.join(globalkeys,
        -- View tag only.
        awful.key({ modkey }, "#" .. i + 9,
                  function ()
                        local screen = awful.screen.focused()
                        local tag = screen.tags[i]
                        if tag then
                           tag:view_only()
                        end
                  end,
                  {description = "view tag #"..i, group = "tag"}),
        -- Toggle tag display.
        awful.key({ modkey, "Control" }, "#" .. i + 9,
                  function ()
                      local screen = awful.screen.focused()
                      local tag = screen.tags[i]
                      if tag then
                         awful.tag.viewtoggle(tag)
                      end
                  end,
                  {description = "toggle tag #" .. i, group = "tag"}),
        -- Move client to tag.
        awful.key({ modkey, "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:move_to_tag(tag)
                          end
                     end
                  end,
                  {description = "move focused client to tag #"..i, group = "tag"}),
        -- Toggle tag on focused client.
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:toggle_tag(tag)
                          end
                      end
                  end,
                  {description = "toggle focused client on tag #" .. i, group = "tag"})
    )
end

clientbuttons = gears.table.join(
    awful.button({ }, 1, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
    end),
    awful.button({ modkey }, 1, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
        awful.mouse.client.move(c)
    end),
    awful.button({ modkey }, 3, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
        awful.mouse.client.resize(c)
    end)
)

-- Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = awful.client.focus.filter,
                     raise = true,
                     keys = clientkeys,
                     buttons = clientbuttons,
                     screen = awful.screen.preferred,
                     callback = function(c)
                           c.maximized, c.maximized_vertical, c.maximized_horizontal = false, false, false
                       end,
                     placement = awful.placement.no_overlap+awful.placement.no_offscreen
     }
    },

    -- Floating clients.
    { rule_any = {
        instance = {
          "DTA",  -- Firefox addon DownThemAll.
          "copyq",  -- Includes session name in class.
          "pinentry",
        },
        class = {
          "URxvt",
          "Blueman-manager",
          "Imager",
          "XTerm",
          "Galculator",
          "Matplotlib",
          "Gnuplot",
          "R_x11",
          "Pavucontrol",
          "Nextcloud",
          "Transmission-gtk",
          "Xarchiver"},

        -- Note that the name property shown in xprop might be set slightly after creation of the client
        -- and the name shown there might not match defined rules here.
        name = {
          "Event Tester",  -- xev.
        },
        role = {
          "AlarmWindow",  -- Thunderbird's calendar.
          "ConfigManager",  -- Thunderbird's about:config.
          "pop-up",       -- e.g. Google Chrome's (detached) Developer Tools.
        }
      }, properties = { floating = true }},

    -- Add titlebars to normal clients and dialogs
    { rule_any = {type = { "normal", "dialog" }
      }, properties = { titlebars_enabled = false }
    },

    -- Set Firefox to always map on the tag named "2" on screen 1.

    { rule = { class = "Alacritty" },
      properties = { tag = "", switchtotag = true } },

    { rule = { class = "Thunar" },
      properties = { tag = "", switchtotag = true } },

    { rule = { class = "Pcmanfm" },
      properties = { tag = "", switchtotag = true } },

    { rule = { class = "Gimp" },
      properties = { tag = "", switchtotag = true } },

    { rule = { class = "Inkscape" },
      properties = { tag = "", switchtotag = true } },

    { rule = { class = "firefox" },
      properties = { tag = "", switchtotag = true } },

    { rule = { instance = "chromium" },
      properties = { tag = "", switchtotag = true } },

    { rule = { class = "LibreWolf" },
      properties = { tag = "", switchtotag = true } },

    { rule = { class = "URxvt" },
      properties = { tag = "" } },

    { rule = { class = "cantata" },
      properties = { tag = "" } },

    { rule = { class = "Clementine" },
      properties = { tag = "" } },

    { rule = { class = "Spotify" },
      properties = { tag = "", switchtotag =true } },

    { rule = { class = "Thunderbird" },
      properties = { tag = "", switchtotag = true } },


    { rule = { class = "TelegramDesktop" },
      properties = { tag = "", switchtotag = true } },


    { rule = { class = "discord" },
      properties = { tag = "", switchtotag = true } },
}
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c)
    -- Set the windows at the slave,
    -- i.e. put it at the end of others instead of setting it master.
    -- if not awesome.startup then awful.client.setslave(c) end

    if awesome.startup
      and not c.size_hints.user_position
      and not c.size_hints.program_position then
        -- Prevent clients from being unreachable after screen count changes.
        awful.placement.no_offscreen(c)
    end
end)

-- Add a titlebar if titlebars_enabled is set to true in the rules.
client.connect_signal("request::titlebars", function(c)
    -- buttons for the titlebar
    local buttons = gears.table.join(
        awful.button({ }, 1, function()
            c:emit_signal("request::activate", "titlebar", {raise = true})
            awful.mouse.client.move(c)
        end),
        awful.button({ }, 3, function()
            c:emit_signal("request::activate", "titlebar", {raise = true})
            awful.mouse.client.resize(c)
        end)
    )

    awful.titlebar(c) : setup {
        { -- Left
            awful.titlebar.widget.iconwidget(c),
            buttons = buttons,
            layout  = wibox.layout.fixed.horizontal
        },
        { -- Middle
            { -- Title
                align  = "center",
                widget = awful.titlebar.widget.titlewidget(c)
            },
            buttons = buttons,
            layout  = wibox.layout.flex.horizontal
        },
        { -- Right
            awful.titlebar.widget.floatingbutton (c),
            awful.titlebar.widget.maximizedbutton(c),
            awful.titlebar.widget.stickybutton   (c),
            awful.titlebar.widget.ontopbutton    (c),
            awful.titlebar.widget.closebutton    (c),
            layout = wibox.layout.fixed.horizontal()
        },
        layout = wibox.layout.align.horizontal
    }
end)

awful.spawn.with_shell("~/.config/awesome/autorun.sh")

-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function(c)
    c:emit_signal("request::activate", "mouse_enter", {raise = false})
end)

client.connect_signal("focus", function(c) 
    c.border_color = beautiful.border_focus
    c.opacity = 1
end)
client.connect_signal("unfocus", function(c) 
    c.border_color = beautiful.border_normal
    c.opacity = 1
end)
-- }}}
