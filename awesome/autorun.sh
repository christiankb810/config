#!/usr/bin/env bash

killall picom
picom &

nitrogen --restore &
