#! /bin/bash
# xcompmgr &

intern=eDP1
extern1=HDMI1
extern2=DP1

if xrandr | grep "$extern1 connected"; then
        xrandr --output "$intern" --off --output "$extern1" --mode 1920x1080
    elif xrandr | grep "$extern2 connected"; then
        xrandr --output "$intern" --off --output "$extern2" --mode 1920x1080
    else
        xrandr --output "$intern" --mode 1366x768
fi

killall picom
picom &

nitrogen --restore &

mopidy --config ~/.config/mopidy/mopidy.conf &

xscreensaver -no-splash &

/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &

xset s off dpms 3600 3600 3600 &
