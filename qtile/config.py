# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from typing import List  # noqa: F401

from libqtile import bar, layout, widget, hook
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal
import os
import subprocess

mod = "mod4"
#terminal = guess_terminal()
terminal="kitty"
#colors=["#0f1108","#DE5283", "#B94870", "#943E5D", "#6E3549", "#492B36", "#242123", "#ffe680"]
colors=["#241c1c","#5c6b73", "#e0fbfc", "#f8c6e1", "#6E3549", "#492B36", "#242123", "#ffe680"]

def window_to_previous_column_or_group(qtile):
    layout = qtile.current_group.layout
    group_index = qtile.groups.index(qtile.current_group)
    previous_group_name = qtile.current_group.get_previous_group().name

    if layout.name != "columns":
        qtile.current_window.togroup(previous_group_name,switch_group=True)
    elif layout.current == 0 and len(layout.cc) == 1:
        if group_index != 0:
            qtile.current_window.togroup(previous_group_name,switch_group=True)
    else:
        layout.cmd_shuffle_left()

def window_to_next_column_or_group(qtile):
    layout = qtile.current_group.layout
    group_index = qtile.groups.index(qtile.current_group)
    next_group_name = qtile.current_group.get_next_group().name

    if layout.name != "columns":
        qtile.current_window.togroup(next_group_name,switch_group=True)
    elif layout.current + 1 == len(layout.columns) and len(layout.cc) == 1:
        if group_index + 1 != len(qtile.groups):
            qtile.current_window.togroup(next_group_name,switch_group=True)
    else:
        layout.cmd_shuffle_right()

keys = [
    Key([mod], "h", lazy.layout.shrink_main()),
    Key([mod], "l", lazy.layout.grow_main()),
    Key([mod], "j", lazy.layout.down()),
    Key([mod], "k", lazy.layout.up()),

    Key([mod, "shift"], "h", lazy.layout.swap_left()),
    Key([mod, "shift"], "l", lazy.layout.swap_right()),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down()),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up()),

    Key([mod, "mod1"], "k", lazy.layout.grow()),
    Key([mod, "mod1"], "j", lazy.layout.shrink()),
    Key([mod], "e", lazy.layout.reset()),
    Key([mod], "m", lazy.layout.maximize()),
    Key([mod, "shift"], "Return", lazy.layout.flip()),

    # Key(["mod1"], "Tab", lazy.layout.next()),
    Key(["mod1", "shift"], "Tab", lazy.layout.prev()),
    Key(["mod1"], "bar", lazy.screen.toggle_group()),

    Key([mod], "Tab", lazy.screen.next_group()),
    Key([mod], "bar", lazy.screen.prev_group()),
    Key([mod, "shift"], "Tab", lazy.function(window_to_next_column_or_group)),
    Key([mod, "shift"], "bar", lazy.function(window_to_previous_column_or_group)),
    # Toggle between different layouts as defined below
    Key([mod], "space", lazy.next_layout()),
    Key([mod, "shift"], "space", lazy.prev_layout()),
    Key([mod], "f", lazy.window.toggle_fullscreen()),
    Key([mod], "s", lazy.window.toggle_floating()),
    Key([mod], "q", lazy.window.kill(), desc="Kill focused window"),
    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "shift"], "Return", lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack"),
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),

    Key([mod], "d", lazy.spawn("rofi -show drun")),
    Key([], "Print", lazy.spawn("xfce4-screenshooter")),
    Key([], "XF86Calculator", lazy.spawn("galculator")),
    Key([], "XF86MonBrightnessDown", lazy.spawn("xbacklight -dec 2 -time 0")),
    Key([], "XF86MonBrightnessUp", lazy.spawn("xbacklight -inc 2 -time 0")),

    Key([], "XF86AudioMute", lazy.spawn("amixer -D pulse sset Master,0 toggle")),
    Key([], "XF86AudioLowerVolume", lazy.spawn("amixer -q -D pulse sset Master 5%-")),
    Key([], "XF86AudioRaiseVolume", lazy.spawn("amixer -q -D pulse sset Master 5%+")),
    Key([], "XF86AudioPlay", lazy.spawn("playerctl play-pause")),
    Key([], "XF86AudioNext", lazy.spawn("playerctl next")),
    Key([], "XF86AudioPrev", lazy.spawn("playerctl previous")),

    Key([mod, "mod1"], "f", lazy.spawn("firefox")),
    Key([mod, "mod1"], "w", lazy.spawn("librewolf")),
    Key([mod, "mod1"], "c", lazy.spawn("chromium")),
    Key([mod, "mod1"], "p", lazy.spawn("PCSX2")),
    Key([mod, "mod1"], "r", lazy.spawn("retroarch")),
    Key([mod, "mod1"], "d", lazy.spawn("discord")),
    Key([mod, "mod1"], "t", lazy.spawn("telegram-desktop")),
    Key([mod, "mod1"], "f", lazy.spawn("firefox")),
    Key([], "XF86Mail", lazy.spawn("thunderbird")),
    Key([mod, "mod1"], "v", lazy.spawn(terminal + " -e vim")),
    Key([mod], "a", lazy.spawn("alacritty -e ranger")),
    Key([mod, "shift"], "a", lazy.spawn("pcmanfm")),
    Key([mod, "mod1"], "a", lazy.spawn("alacritty -e sh ./.config/vifm/scripts/vifmrun")),
    Key([mod], "w", lazy.spawn("./.config/qtile/rofi_scripts/menu.sh")),
    Key([], "XF86Tools", lazy.spawn("urxvt -e ncmpcpp")),
    # Toggle between different layouts as defined below

    Key([mod, "control"], "r", lazy.restart(), desc="Restart Qtile"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    Key([mod], "r", lazy.spawncmd(),
        desc="Spawn a command using a prompt widget"),
]

group_labels=[
        "", "", "", "", "", "",
        ]

group_names=[
        "1", "2", "3", "4", "5", "6"
        ]

group_exclusives=[
        False, False, False, False, False, False,
        ]

group_persists=[
        True, True, True, True, True, True,
        ]

group_inits=[
        True, True, True, True, True, True
        ]

group_layouts=[
        "monadtall", "treetab", "monadtall", "monadtall", "monadtall", "monadtall",
        ]

group_matches=[
        None,
        [Match(wm_class=[
            "Alacritty", "thunar", "pcmanfm"
            ])],
        [Match(wm_class=[
            "Inkscape", "Gimp-2.10",
            ])],
        [Match(wm_class=[
            "firefox", "Chromium", "LibreWolf",
            ]), ],
        [Match(wm_class=[
            "URxvt", "cantata", "Spotify",
            ]), ],
        [Match(wm_class=[
            "Thunderbird", "TelegramDesktop", "discord",
            ]), ],
        ]

groups = []

for i in range(len(group_names)):
    groups.append(
            Group(
                name=group_names[i],
                matches=group_matches[i],
                exclusive=group_exclusives[i],
                layout=group_layouts[i],
                persist=group_persists[i],
                init=group_inits[i],
                label=group_labels[i],
                )
            )

for i in groups:
    keys.extend([
        # mod1 + letter of group = switch to group
        Key([mod], i.name, lazy.group[i.name].toscreen(),
            desc="Switch to group {}".format(i.name)),

        # mod1 + shift + letter of group = switch to & move focused window to group
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name, switch_group=True),
            desc="Switch to & move focused window to group {}".format(i.name)),
        # Or, use below if you prefer not to switch to that group.
        # # mod1 + shift + letter of group = move focused window to group
        # Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
        #     desc="move focused window to group {}".format(i.name)),
    ])

layout_theme={
        "border_width": 2,
        "margin": 4,
        "border_focus": colors[2],
        "border_normal": "#1D2330"
        }

layouts = [
    layout.MonadTall(
        **layout_theme,
        align = 0,
        max_ratio=0.70,
        min_ratio=0.30,
        ),
    # layout.Bsp(
    #     **layout_theme,
    #     fair = False,
    #     ),
    # layout.Stack(num_stacks=2,
    #     **layout_theme),
    # Try more layouts by unleashing below layouts.
    # layout.Columns(),
    # layout.Matrix(),
    # layout.RatioTile(),
    layout.TreeTab(
        panel_width = 120,
        padding_left = 0,
        padding_x = 2,
        fontsize = 11,
        bg_color = colors[0],
        active_bg = colors[2],
        active_fg = colors[0],
        inactive_bg = colors[1],
        inactive_fg = colors[0],
        previous_on_rm=True,
        ),
    # layout.VerticalTile(),
    layout.Max(),
    layout.MonadWide(
        **layout_theme,
        align = 0,
        new_at_current = True
        ),
    # layout.Zoomy(),
]

widget_defaults = dict(
    font='Cantarell',
    fontsize=12,
    padding=3,
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.GroupBox(
                    this_current_screen_border = colors[2],
                    highlight_method = 'line',
                    highlight_color = colors[0],
                    borderwidth = 2,
                    urgent_alert_method='line',
                    urgent_border=colors[7],
                    padding_x = 12,
                    font = 'Iosevka Nerd Font',
                    rounded=False,
                    fontsize = 14,
                    margin = 1,
                    ),
                widget.CurrentLayout(
                    foreground = colors[1],
                    ),
                widget.Prompt(),
                widget.Spacer(),
                widget.Systray(
                    padding=5,
                    ),
                widget.Spacer(
                   length = 6,
                    ),
#                 widget.Notify(
#                     foreground=colors[3],
#                     foreground_low=colors[4],
#                     default_timeout=5,
#                     ),
                widget.TextBox(
                    text = '',
                    foreground=colors[2],
                    #background = colors[2],
                    padding = 4,
                    ),
                widget.CPU(
                    #background=colors[2],
                    #foreground=colors[1],
                    padding=4,
                    format='{freq_current}GHz {load_percent}%',
                    ),
                widget.TextBox(
                    text = '',
                    foreground=colors[2],
                    #background = colors[3],
                    padding = 4,
                    ),
                widget.Memory(
                    #background=colors[3],
                    #foreground=colors[1],
                    measure_mem='G',
                    format='{MemUsed: .1f}{mm}/{MemTotal: .1f}{mm}',
                    padding=4,
                    ),
                widget.TextBox(
                    text = '',
                    #background = colors[4],
                    foreground=colors[2],
                    padding = 2,
                    ),
                widget.Wlan(
                    #background = colors[4],
                    #foreground=colors[1],
                    #format='{essid} {percent:2.0%}',
                    format='{essid}',
                    padding = 2,
                    interface='wlp2s0',
                    ),
                # widget.Battery(
                #     foreground = colors[2],
                #     charge_char = "",
                #     discharge_char = "",
                #     empty_char = "",
                #     full_char = "",
                #     format = '{char}',
                #     show_short_text = False,
                #     ),
                # widget.Battery(
                #     foreground = colors[1],
                #     format = '{percent:2.0%}',
                #     padding = 4,
                #     ),
                # widget.Spacer(
                #    length = 2,
                #     ),
                # widget.TextBox(
                #     text = '',
                #     foreground = colors[2],
                #     padding = 4,
                #     ),
                # widget.Backlight(
                #     backlight_name = "intel_backlight",
                #     foreground = colors[1],
                #     padding = 4,
                #     ),
                # widget.Spacer(
                #    length = 2,
                #     ),
                # widget.TextBox(
                #     text = '',
                #     background = colors[0],
                #     foreground = colors[1],
                #     padding = 0,
                #     fontsize = 20,
                widget.TextBox(
                    text = '',
                    #background = colors[5],
                    foreground=colors[2],
                    padding = 4,
                    ),
                widget.PulseVolume(
                    #background = colors[5],
                    #foreground=colors[1],
                    volume_app = "pavucontrol",
                    padding = 4,
                    step = 5,
                    ),
                widget.TextBox(
                    text = '\ue0ba',
                    background = colors[0],
                    foreground = colors[2],
                    padding = 0,
                    fontsize = 20,
                    ),
                widget.Clock(
                    background = colors[2],
                    foreground = colors[0],
                    format='%a %d %b,  %I:%M %p',
                    padding = 5,
                    ),
            ],
            22,
            background= colors[0],
            opacity=0.96,
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
follow_mouse_focus = False
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
    Match(wm_class='confirmreset'),  # gitk
    Match(wm_class='makebranch'),  # gitk
    Match(wm_class='maketag'),  # gitk
    Match(wm_class='ssh-askpass'),  # ssh-askpass
    Match(wm_class='R_x11'),  # ssh-askpass
    Match(wm_class='matplotlib'),  # ssh-askpass
    Match(wm_class='gnuplot'),  # ssh-askpass
    Match(wm_class='gnuplot_qt'),  # ssh-askpass
    Match(wm_class='Imager'),  # ssh-askpass
    Match(wm_class='URxvt'),  # ssh-askpass
    Match(wm_class='galculator'),  # ssh-askpass
    Match(wm_class='xfce4-screenshooter'),  # ssh-askpass
    Match(wm_class='Pavucontrol'),  # ssh-askpass
    Match(wm_class='xarchiver'),  # ssh-askpass
    Match(wm_class='Blueman-manager'),  # ssh-askpass
    Match(wm_class='transmission-gtk'),  # ssh-askpass
    Match(title='branchdialog'),  # gitk
    Match(title='pinentry'),  # GPG key password entry
    Match(title='bluetoothctl'),  # GPG key password entry
    Match(title='Imprimir'),  # GPG key password entry
],
        border_focus = colors[1],
        border_normal = "#1D2330"
)
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

floating_types = ["notification", "toolbar", "splash", "dialog",
                  "utility", "menu", "dropdown_menu", "popup_menu", "tooltip,dock",
                ]
# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"

@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser('~/.config/qtile/autostart.sh')
    subprocess.call([home])

@hook.subscribe.client_new
def set_floating(window):
    if (window.window.get_wm_transient_for()
            or window.window.get_wm_type() in floating_types):
        window.floating = True

    for group in groups:  # follow on auto-move
        match = next((m for m in group.matches if m.compare(window)), None)
        if match:
            targetgroup = window.qtile.groups_map[group.name]  # there can be multiple instances of a group
            targetgroup.cmd_toscreen(toggle=False)
            break
