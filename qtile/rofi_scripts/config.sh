#!/bin/bash

# Custom Rofi Script

# Launch Rofi
MENU="$(rofi -no-lazy-grab -sep "|" -dmenu -i -p 'Configuracion :' \
-theme "~/.config/rofi/slate-menu.rasi" \
-lines 4 \
<<< "   Qtile|  Autostart|  Xscreensaver|  Sonido")"
case "$MENU" in
  *Qtile) kitty -e vim  ~/.config/qtile/config.py;;
  *Autostart) kitty -e vim  ~/.config/qtile/autostart.sh;;
  *Xscreensaver) xscreensaver-demo;;
  *Sonido) pavucontrol;;
esac

