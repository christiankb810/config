#!/bin/bash

# Custom Rofi Script

# Launch Rofi
MENU="$(rofi -no-lazy-grab -sep "|" -dmenu -i -p 'Menu:' \
-theme "~/.config/rofi/slate-menu.rasi" \
-lines 3 \
<<< "   Sesión|  Conección|  Configuración")"
case "$MENU" in
  *Sesión) ~/.config/qtile/rofi_scripts/system.sh;;
  *Conección) ~/.config/qtile/rofi_scripts/coneccion.sh;;
  *Configuración) ~/.config/qtile/rofi_scripts/config.sh;;
esac

