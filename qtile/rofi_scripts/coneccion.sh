#!/bin/bash

# Custom Rofi Script

# Launch Rofi
MENU="$(rofi -no-lazy-grab -sep "|" -dmenu -i -p 'Coneccion :' \
-theme "~/.config/rofi/slate-menu.rasi" \
-lines 2 \
<<< "   Wifi|  Bluetooth")"
case "$MENU" in
  *Wifi) networkmanager_dmenu;;
  *Bluetooth) xterm -e bluetoothctl;;
esac
