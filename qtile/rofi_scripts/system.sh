#!/bin/bash

# Custom Rofi Script

# Launch Rofi
MENU="$(rofi -no-lazy-grab -sep "|" -dmenu -i -p 'System :' \
-theme "~/.config/rofi/slate-menu.rasi" \
-lines 5 \
<<< "   Apagar|  Reiniciar|  Suspender|   Salir|  Bloquear")"
case "$MENU" in
  *Apagar) systemctl -i poweroff;;
  *Reiniciar) systemctl reboot;;
  *Suspender) systemctl suspend;;
  *Salir) pkill qtile;;
  *Bloquear) xscreensaver-command -lock
esac

